// use std::collections::HashMap;
// use std::hash::Hash;

fn id<T>(x: T) -> T {
    x
}

fn compose<A, B, C, G, F>(f: F, g: G) -> impl Fn(A) -> C where
    G : Fn(A) -> B,
    F : Fn(B) -> C
{
    move |a| f(g(a))
}


// fn memoize<A, B, F>(f: F, cache: &'static mut HashMap<A, B>) -> impl FnMut(A) -> B where 
//     A : Eq + Hash + Copy,
//     B : Clone, 
//     F : Fn(A) -> B
// {
//     move |value: A| {
//         if !cache.contains_key(&value) {
//             cache.insert(
//                 value,
//                 f(value.clone())
//             );
//         }
//         let res = cache.get(&value).unwrap();
//         res.clone()
//     }
// }

#[derive(Debug)]
enum Optional<T> {
    Valid(T),
    Invalid
}

fn kleisli_compose<A, B, C, F, G>(f: F, g: G) -> impl Fn(A) -> Optional<C> where
    F : Fn(A) -> Optional<B>,
    G : Fn(B) -> Optional<C>
{
    move |a| {
        match f(a) {
            Optional::Valid(b)  => { return g(b) },
            Invalid             => Optional::Invalid
        }
    }
}

fn safe_root(x : f32) -> Optional<f32> {
    if x <= 0.0 { return Optional::Invalid }
    Optional::Valid(x.sqrt())
}

fn safe_reciprocal(x : f32) -> Optional<f32> {
    if x == 0.0 { return Optional::Invalid }
    Optional::Valid(1.0 / x)
}


// chapter 5
#[derive(Debug, PartialEq, Clone)]
enum Either<A, B> {
    Left (A),
    Right(B)
}

fn factorizer<A, B, C, F, G>(f: F, g: G) -> impl Fn(Either<A, B>) -> C where 
    F : Fn(A) -> C,
    G : Fn(B) -> C
{
    move |x| {
        match x {
            Either::Left (a) => return f(a),
            Either::Right(b) => return g(b),
        }
    }
}


fn i(x: i32) -> i32 {x}
fn j(b: bool) -> i32 {
    match b {
        true    => return 1,
        false   => return 0
    }
}


//chaper 6
enum Maybe<a> {
    Just(a),
    Nothing
}

fn toMaybe<a>(val : Either<(), a>) -> Maybe<a> {
    match val {
        Either::Left(_) => return Maybe::Nothing,
        Either::Right(x)=> return Maybe::Just(x),
    }
}

fn toEither<a>(val: Maybe<a>) -> Either<(), a> {
    match val {
        Maybe::Just(x) => return Either::Right(x),
        Maybe::Nothing => return Either::Left(()),
    }
}

enum Shape {
    Circle  (f32),
    Rect    (f32, f32),
    Square  (f32)
}

fn area(shape: Shape) -> f32 {
    match shape {
        Shape::Circle(r)    => return 3.14 * r * r,
        Shape::Rect  (b, h) => return b * h,
        Shape::Square(s)    => return s * s
    }
}

fn circ(shape: Shape) -> f32 {
    match shape {
        Shape::Circle(r)    => return 2.0 * 3.14 * r,
        Shape::Rect  (b, h) => return 2.0 * (b + h),
        Shape::Square(s)    => return 4.0 * s
    }
}

// chapter 7, functors
// pub trait Functor<A, B, Constructor, C1, C2> where 
//     C1 : Constructor<A>,
//     C2 : Constructor<B>
//     {
//     fn fmap(a : Fn(A)->B) -> Fn(Constructor<A>) -> Constructor<B>;
// }
// pub trait Functor {
// fn fmap<A, B, F, C1, C2>(f : F) -> Fn(C1) -> C2  where 
//     F : Fn(A)->B ;
// }



trait Functor <A, B, C, F> where F : Fn(&A) -> B {
    fn fmap(&self, f: F) ->  C;
}

trait Bifunctor<A, B, C, D, Func, FuncFst, FuncSnd, F, G> where 
    F : Fn(&A) -> B,
    G : Fn(&C) -> D {
    fn bimap (&self, f: F, g: G) -> Func;
    fn first (&self, f: F)       -> FuncFst;
    fn second(&self, g: G)       -> FuncSnd;
}

enum List<T> {
    Nil,
    List(T,  Box<List<T>>)
}

impl<A, B, F> Functor<A, B, List<B>, F> for List<A> 
where F : Fn(&A) -> B {
    fn fmap(&self, f: F) -> List<B> {
        match self {
            List::Nil => {return List::Nil},
            List::List(v, next) => {
                List::List(
                    f(v),
                    Box::new(next.fmap(f))
                )
            }
        }
    }
}

enum Reader<A, B> {
    _x_(A, B)
}

enum BiTuple<A, B> {
    Values(A, B)
}

impl<A, B, C, D, F, G> Bifunctor<
    A, B, C, D, 
    BiTuple<B, D>, 
    BiTuple<B, C>, 
    BiTuple<A, D>, 
    F, G> for BiTuple<A, C> where
    F : Fn(&A) -> B,
    G : Fn(&C) -> D,
    A : Clone,
    C : Clone {
        fn bimap(&self, f: F, g: G) -> BiTuple<B, D> {
            match self {
                BiTuple::Values(v1, v2) => BiTuple::Values(f(v1), g(v2))
            }
        }
        fn first(&self, f : F) -> BiTuple<B, C> {
            match self {
                BiTuple::Values(a, c) => BiTuple::Values(f(a), c.clone())
            }
        }
        fn second(&self, g: G) -> BiTuple<A, D> {
            match self {
                BiTuple::Values(a, c) => BiTuple::Values(a.clone(), g(c))
            }
        }
}

// impl<A, B, R, FB, F> Functor<A, B, FB, F> for Reader<R, A> where 
//     F : Fn(&A) -> B,
//     FB: Fn(&R) -> B{
//     fn fmap(&self, f: F) -> FB {
        
//     }
// }

fn main() {
    let x = id(&5);
    println!("{}", x);
    let f = |&a| { a + 5};
    let g = id;
    let fg = compose(f, g);
    let res = fg(x);
    println!("{}", res);

    // let f2 = |x : i32| {x + 5};
    //chapter 5
    let kleisli_test = kleisli_compose(safe_root, safe_reciprocal);
    println!("{:?}", kleisli_test(4.0));

    //chaper 6
    //proofe isomorphism
    let testVal: Either<(), i32> = Either::Right(10);
    assert_eq!(testVal, toEither(toMaybe(testVal.clone())));
    //a+a = 2*a
    //2 = 1+1 => 2*a = (1+1)*a = a+a
    let l : List<i32> = List::List(
        10, 
        Box::new(List::List(
            5, 
            Box::new(List::Nil))));
}
